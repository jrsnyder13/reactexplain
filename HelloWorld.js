React.renderComponent(
    React.DOM.div({className: "containerDiv"},
        React.DOM.h1(null, "Hello, World!"),
        React.DOM.input({
            value: "Input box"
        })
    ),
    document.getElementById("reactMount")
);