var TodoList = React.createClass({ displayName: 'TodoList',
    propTypes: {
        items: React.PropTypes.arrayOf( React.PropTypes.string ).isRequired,
        deleteItem: React.PropTypes.func.isRequired
    },
    render: function() {
        return React.DOM.ul(null,
            this.props.items.map(function(itemName, index) {
                return React.DOM.li(null,
                    itemName,
                    React.DOM.button({
                        onClick: function() {
                            this.props.deleteItem(index);
                        }
                    },
                    "X")
                );
            }, this)
        );
    }
});


var TodoApp = React.createClass({ displayName: 'TodoApp',
    getInitialState: function() {
        return {
            items: [],
            newItemName: ''
        };
    },
    render: function() {
        return (
            React.DOM.div(null,
                React.DOM.h3(null, "TODO"),
                TodoList({
                    items: this.state.items,
                    deleteItem: this.deleteItem
                }),
                React.DOM.form( {onSubmit: this.handleSubmit},
                    React.DOM.input({
                        onChange: this.nameChange,
                        value: this.state.newItemName
                    }),
                    React.DOM.input({
                        type: "submit",
                        value: 'Add #' + (this.state.items.length + 1)
                    })
                )
            )
        );
    },
    nameChange: function(event) {
        this.setState({newItemName: event.target.value});
    },
    handleSubmit: function(event) {
        event.preventDefault();
        this.setState({
            items: this.state.items.concat(this.state.newItemName),
            newItemName: ''
        });
    },
    deleteItem: function(itemIndex) {
        this.state.items.splice(itemIndex, 1);
        this.forceUpdate();
    }
});


React.renderComponent(
    TodoApp(),
    document.getElementById("reactMount")
);